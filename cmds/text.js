// Mappings
const bubblemap = {
    A: "\u24B6",
    B: "\u24B7",
    C: "\u24B8",
    D: "\u24B9",
    E: "\u24BA",
    F: "\u24BB",
    G: "\u24BC",
    H: "\u24BD",
    I: "\u24BE",
    J: "\u24BF",
    K: "\u24C0",
    L: "\u24C1",
    M: "\u24C2",
    N: "\u24C3",
    O: "\u24C4",
    P: "\u24C5",
    Q: "\u24C6",
    R: "\u24C7",
    S: "\u24C8",
    T: "\u24C9",
    U: "\u24CA",
    V: "\u24CB",
    W: "\u24CC",
    X: "\u24CD",
    Y: "\u24CE",
    Z: "\u24CF",

    a: "\u24D0",
    b: "\u24D1",
    c: "\u24D2",
    d: "\u24D3",
    e: "\u24D4",
    f: "\u24D5",
    g: "\u24D6",
    h: "\u24D7",
    i: "\u24D8",
    j: "\u24D9",
    k: "\u24DA",
    l: "\u24DB",
    m: "\u24DC",
    n: "\u24DD",
    o: "\u24DE",
    p: "\u24DF",
    q: "\u24E0",
    r: "\u24E1",
    s: "\u24E2",
    t: "\u24E3",
    u: "\u24E4",
    v: "\u24E5",
    w: "\u24E6",
    x: "\u24E7",
    y: "\u24E8",
    z: "\u24E9",

    "0": "\u24EA",
    "1": "\u2460",
    "2": "\u2461",
    "3": "\u2462",
    "4": "\u2463",
    "5": "\u2464",
    "6": "\u2465",
    "7": "\u2466",
    "8": "\u2467",
    "9": "\u2468",

    " ": "\u25EF"
};

const rDance = {
    "0": "<a:r_letter_0:393623734092693524>",
    "1": "<a:r_letter_1:393623734344220673>",
    "2": "<a:r_letter_2:393623734180642837>",
    "3": "<a:r_letter_3:393623734109601812>",
    "4": "<a:r_letter_4:393623734147088384>",
    "5": "<a:r_letter_5:393623733731983362>",
    "6": "<a:r_letter_6:393623733962539019>",
    "7": "<a:r_letter_7:393623734075785216>",
    "8": "<a:r_letter_8:393623734155739136>",
    "9": "<a:r_letter_9:393623734101213194>",
    a: "<a:r_letter_a:393623734344351744>",
    b: "<a:r_letter_b:393623734034104322>",
    c: "<a:r_letter_c:393623734176448512>",
    d: "<a:r_letter_d:393623734168190996>",
    e: "<a:r_letter_e:393623733937373185>",
    f: "<a:r_letter_f:393623734268985346>",
    g: "<a:r_letter_g:393623734055075841>",
    h: "<a:r_letter_h:393623734549741568>",
    i: "<a:r_letter_i:393623734616981514>",
    j: "<a:r_letter_j:393623734323249174>",
    k: "<a:r_letter_k:393623734461792257>",
    l: "<a:r_letter_l:393623734621306881>",
    m: "<a:r_letter_m:393623734813982720>",
    n: "<a:r_letter_n:393623734696542209>",
    o: "<a:r_letter_o:393623735011246080>",
    p: "<a:r_letter_p:393623734931554305>",
    q: "<a:r_letter_q:393623735137206272>",
    r: "<a:r_letter_r:393623735065903114>",
    s: "<a:r_letter_s:393623735132749824>",
    t: "<a:r_letter_t:393623735002988545>",
    u: "<a:r_letter_u:393623735229218816>",
    v: "<a:r_letter_v:393623735216898049>",
    w: "<a:r_letter_w:393623735388602368>",
    x: "<a:r_letter_x:393623735028154369>",
    y: "<a:r_letter_y:393623735233675265>",
    z: "<a:r_letter_z:393623735376150550>",
    "&": "<a:r_letter_and:393623733761212427>",
    "@": "<a:r_letter_at:393623734122053632>",
    $: "<a:r_letter_dollar:393623734344351774>",
    "!": "<a:r_letter_exclaim:393623734377906176>"
};
const redDance = {
    "0": "<a:dancing_0:469268028908109824>",
    "1": "<a:dancing_1:469268028576759820>",
    "2": "<a:dancing_2:469268028534685707>",
    "3": "<a:dancing_3:469268029063299072>",
    "4": "<a:dancing_4:469268029121757234>",
    "5": "<a:dancing_5:469268029348249611>",
    "6": "<a:dancing_6:469268028966567946>",
    "7": "<a:dancing_7:469268029058842634>",
    "8": "<a:dancing_8:469268030006886410>",
    "9": "<a:dancing_9:469268028962635791>",
    a: "<a:dancing_a:469268029113368577>",
    b: "<a:dancing_b:469268029700702238>",
    c: "<a:dancing_c:469268029708959744>",
    d: "<a:dancing_d:469268029717479424>",
    e: "<a:dancing_e:469268029524541451>",
    f: "<a:dancing_f:469268030048698392>",
    g: "<a:dancing_g:469268030309007381>",
    h: "<a:dancing_h:469268030313070624>",
    i: "<a:dancing_i:469268030145167361>",
    j: "<a:dancing_j:469268030212538369>",
    k: "<a:dancing_k:469268030283710466>",
    l: "<a:dancing_l:469268030543626240>",
    m: "<a:dancing_m:469268030686494720>",
    n: "<a:dancing_n:469268030636032000>",
    o: "<a:dancing_o:469268031143542785>",
    p: "<a:dancing_p:469268030942347264>",
    q: "<a:dancing_q:469268030799740958>",
    r: "<a:dancing_r:469268031479087124>",
    s: "<a:dancing_s:469268031470698522>",
    t: "<a:dancing_t:469268031382749205>",
    u: "<a:dancing_u:469268031441207326>",
    v: "<a:dancing_v:469268031487475732>",
    w: "<a:dancing_w:469268031584075776>",
    x: "<a:dancing_x:469290974498258944>",
    y: "<a:dancing_y:469268031478956043>",
    z: "<a:dancing_z:469268031462440972>",
    "&": "<a:dancing_and:469268029365026816>",
    "@": "<a:dancing_at:469268029486661653>",
    $: "<a:dancing_dollar:469268029579198465>",
    "!": "<a:dancing_exclaim:469268029985783810>",
    "?": "<a:dancing_question:469268030757535755>"
};
const fastDance = {
    "0": "<a:00:478301208290000897>",
    "1": "<a:11:478301208453447711>",
    "2": "<a:22:478301208717557791>",
    "3": "<a:33:478301208398921748>",
    "4": "<a:44:478301208696848394>",
    "5": "<a:55:478301208499716097>",
    "6": "<a:66:478301208470224916>",
    "7": "<a:77:478301208675745812>",
    "8": "<a:88:478301208365236226>",
    "9": "<a:99:478301209330057218>",
    a: "<a:aa:478301209212616705>",
    b: "<a:bb:478301209372131348>",
    c: "<a:cc:478301209287983124>",
    d: "<a:dd:478301209023873054>",
    e: "<a:ee:478301209250365460>",
    f: "<a:ff:478301209221005322>",
    g: "<a:gg:478301209548161025>",
    h: "<a:hh:478301208981929984>",
    i: "<a:ii:478301209502023680>",
    j: "<a:jj:478301210076774449>",
    k: "<a:kk:478301209497960458>",
    l: "<a:ll:478301209246040065>",
    m: "<a:mm:478301209736904715>",
    n: "<a:nn:478301209199902731>",
    o: "<a:oo:478301209480921099>",
    p: "<a:pp:478301209585778700>",
    q: "<a:qq:478301207652335637>",
    r: "<a:rr:478301209292177419>",
    s: "<a:ss:478301209506086943>",
    t: "<a:tt:478301209435045888>",
    u: "<a:uu:478301209581584414>",
    v: "<a:vv:478301209619464195>",
    w: "<a:ww:478301209480921088>",
    x: "<a:xx:478301209506217984>",
    y: "<a:yy:478301209476726784>",
    z: "<a:zz:478301209514475520>"
};
const logos = {
    "0": "<:Logo0:415342593321205760>",
    "1": "<:Logo1:415342593313079316>",
    "2": "<:Logo2:415342593166016524>",
    "3": "<:Logo3:465620480129499136>",
    "4": "<:Logo4:415342593094844417>",
    "5": "<:Logo5:415342593564737546>",
    "6": "<:Logo6:415342593250164754>",
    "7": "<:Logo7:415342593455554560>",
    "8": "<:Logo8:415342593313079298>",
    "9": "<:Logo9:415342593556086784>",
    a: "<:LogoA:414655543458922506>",
    b: "<:LogoB:414655554158329856>",
    c: "<:LogoC:414655566636646400>",
    d: "<:LogoD:414655751181565974>",
    e: "<:LogoE:414655783116996608>",
    f: "<:LogoF:414655803157381122>",
    g: "<:LogoG:414655817493512200>",
    h: "<:LogoH:414655829732753420>",
    i: "<:LogoI:414656712478294026>",
    j: "<:LogoJ:414660430577795072>",
    k: "<:LogoK:414660451822075904>",
    l: "<:LogoL:414660470834724876>",
    m: "<:LogoM:414660488970895360>",
    n: "<:LogoN:414660515298541589>",
    o: "<:LogoO:414660533531181057>",
    p: "<:LogoP:414676183926571009>",
    q: "<:LogoQ:414666225600299018>",
    r: "<:LogoR:414666249138601994>",
    s: "<:LogoS:414666264930418698>",
    t: "<:LogoT:414666283687346187>",
    u: "<:LogoU:414666299407335424>",
    v: "<:LogoV:414666317627523073>",
    w: "<:LogoW:414666346144595978>",
    x: "<:LogoX:414666361373982722>",
    y: "<:LogoY:414666374179454976>",
    z: "<:LogoZ:414666386304925706>",
    "!": "<:LogoExclam:466089799338426378>",
    ".": "<:LogoPeriod:466089797027627008>"
};

const riMap = {
    "0": ":zero:",
    "1": ":one:",
    "2": ":two:",
    "3": ":three:",
    "4": ":four:",
    "5": ":five:",
    "6": ":six:",
    "7": ":seven:",
    "8": ":eight:",
    "9": ":nine:",
    a: ":regional_indicator_a:",
    b: ":regional_indicator_b:",
    c: ":regional_indicator_c:",
    d: ":regional_indicator_d:",
    e: ":regional_indicator_e:",
    f: ":regional_indicator_f:",
    g: ":regional_indicator_g:",
    h: ":regional_indicator_h:",
    i: ":regional_indicator_i:",
    j: ":regional_indicator_j:",
    k: ":regional_indicator_k:",
    l: ":regional_indicator_l:",
    m: ":regional_indicator_m:",
    n: ":regional_indicator_n:",
    o: ":regional_indicator_o:",
    p: ":regional_indicator_p:",
    q: ":regional_indicator_q:",
    r: ":regional_indicator_r:",
    s: ":regional_indicator_s:",
    t: ":regional_indicator_t:",
    u: ":regional_indicator_u:",
    v: ":regional_indicator_v:",
    w: ":regional_indicator_w:",
    x: ":regional_indicator_x:",
    y: ":regional_indicator_y:",
    z: ":regional_indicator_z:",
    "#": ":hash:",
    "*": ":asterisk:"
};

// Commands

let dancesay = function(ctx, msg, args) {
    if (!args) {
        msg.channel.createMessage(`Please use with some text.`);
    } else {
        let isRed = args.startsWith("--red ");
        let isFast = args.startsWith("--fast ");
        args = args.startsWith("--red ")
            ? args.replace("--red ", "")
            : args.startsWith("--fast ") ? args.replace("--fast ", "") : args;

        let inp = ctx.utils.safeString(args).split("");
        let out = "";
        for (let x in inp) {
            if (inp[x].toLowerCase() == " " || inp[x].toLowerCase() == ":") {
                out += "<:blankboi:393555375389016065>";
            } else {
                out +=
                    (isRed
                        ? redDance[inp[x].toLowerCase()]
                        : isFast
                          ? fastDance[inp[x].toLowerCase()]
                          : rDance[inp[x].toLowerCase()]) ||
                    inp[x].toLowerCase();
            }
        }

        msg.channel.createMessage(out);
    }
};

let fullwidth = function(ctx, msg, args) {
    if (!args) {
        msg.channel.createMessage(`Please use with some text.`);
    } else {
        let inp = ctx.utils.safeString(args).split("");
        let out = inp
            .map(
                x =>
                    x == " "
                        ? "\u3000"
                        : String.fromCodePoint(x.charCodeAt() + 0xfee0)
            )
            .join("");

        msg.channel.createMessage(out);
    }
};

let bunnysay = function(ctx, msg, args) {
    let cside = "\uFF5C";
    let ctop = "\uFFE3";
    let cbot = "\uFF3F";

    let bun = "(\\__/) ||\n(\u2022\u3145\u2022) ||\n/ \u3000 \u3065||";

    if (!args) {
        let err = "Please use with some text.";
        let inp = ctx.utils.safeString(err).split("");
        let out = inp
            .map(
                x =>
                    x == " "
                        ? "\u3000"
                        : String.fromCodePoint(x.charCodeAt() + 0xfee0)
            )
            .join("");

        let final = "";
        final += cside + ctop.repeat(err.length) + cside + "\n";
        final += cside + out + cside + "\n";
        final += cside + cbot.repeat(err.length) + cside + "\n";
        final += bun;

        msg.channel.createMessage(`\`\`\`${final}\`\`\``);
    } else {
        let inp = ctx.utils.safeString(args).split("");
        let out = inp
            .map(
                x =>
                    x == " "
                        ? "\u3000"
                        : String.fromCodePoint(x.charCodeAt() + 0xfee0)
            )
            .join("");

        let final = "";
        final += cside + ctop.repeat(args.length) + cside + "\n";
        final += cside + out + cside + "\n";
        final += cside + cbot.repeat(args.length) + cside + "\n";
        final += bun;

        msg.channel.createMessage(`\`\`\`${final}\`\`\``);
    }
};

let bubblesay = function(ctx, msg, args) {
    if (!args) {
        msg.channel.createMessage(`Please use with some text.`);
    } else {
        let inp = ctx.utils.safeString(args).split("");
        let out = "";
        for (let x in inp) {
            out += bubblemap[inp[x]] || inp[x];
        }

        msg.channel.createMessage(out);
    }
};

let logosay = function(ctx, msg, args) {
    if (!args) {
        msg.channel.createMessage(`Please use with some text.`);
    } else {
        let inp = ctx.utils.safeString(args).split("");
        let out = "";
        for (let x in inp) {
            if (inp[x].toLowerCase() == " " || inp[x].toLowerCase() == ":") {
                out += "<:blankboi:393555375389016065>";
            } else {
                out += logos[inp[x].toLowerCase()] || inp[x].toLowerCase();
            }
        }

        msg.channel.createMessage(out);
    }
};

let ri = function(ctx, msg, args) {
    if (!args) {
        msg.channel.createMessage(`Please use with some text.`);
    } else {
        let inp = ctx.utils.safeString(args).split("");
        let out = "";
        for (let x in inp) {
            if (inp[x].toLowerCase() == " " || inp[x].toLowerCase() == ":") {
                out += "<:blankboi:393555375389016065>";
            } else {
                out += riMap[inp[x].toLowerCase()] || inp[x].toLowerCase();
            }
        }

        msg.channel.createMessage(out);
    }
};

module.exports = [
    {
        name: "dancesay",
        desc:
            "hey its that dancing letter meme, put `--red` before text to make it red letters and `--fast` for fast red letters.",
        func: dancesay,
        group: "text",
        aliases: ["dsay", "ds"]
    },
    {
        name: "fullwidth",
        desc:
            "\uFF41\uFF45\uFF53\uFF54\uFF48\uFF45\uFF54\uFF49\uFF43\u3000\uFF54\uFF45\uFF58\uFF54",
        func: fullwidth,
        group: "text",
        aliases: ["fw"]
    },
    {
        name: "bunnysay",
        desc: "Make a bunny say things",
        func: bunnysay,
        group: "text",
        aliases: ["bunsay"]
    },
    {
        name: "bubblesay",
        desc: "Just don't let them pop",
        func: bubblesay,
        group: "text",
        aliases: ["bsay"]
    },
    {
        name: "logosay",
        desc: "Say things in logos",
        func: logosay,
        group: "text",
        aliases: ["logo"]
    },
    {
        name: "regional",
        desc: "Say things in regional indicators. For the lazy.",
        func: ri,
        group: "text",
        aliases: ["bigtext", "ri"]
    }
];
