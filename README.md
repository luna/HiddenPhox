# HiddenPhox
HiddenPhox is a kitchen sink bot of sorts providing utilities, moderation tools and fun.

[![Discord Bots](https://discordbots.org/api/widget/152172984373608449.svg)](https://discordbots.org/bot/152172984373608449)

## Features
* Various lookup utility commands such as user, role, guild, invites, emotes, etc.
* Basic image editing, mirroring and flipping.
* Rich and visually appealing music system, supporting YouTube, Soundcloud, playlists and direct files along with dynamic volume.
* Text based commands, fullwidth, dancing letters, bubbles, bunny with a sign.
* Economy, steal from other users to get to the top of the leaderboards.
* Moderation commands for quick and easy banning or kicking.
* Self assignable roles, never be bothered by someone asking for a role again.